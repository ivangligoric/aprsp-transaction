create table public.transaction
(
    id serial primary key,
    date_created timestamp without time zone NOT NULL,
    purpose character varying(128),
    amount numeric NOT NULL,
    bank_account_sender character varying(128) NOT NULL,
    bank_account_receiver character varying(128) NOT NULL,
	
    constraint transaction_pkey primary key (id)
)

create index pk_transaction on public.transaction(id);

insert into public.transaction (purpose, amount, bank_account_sender, bank_account_receiver)
values ('poklon', 10000, '11111', '22222')