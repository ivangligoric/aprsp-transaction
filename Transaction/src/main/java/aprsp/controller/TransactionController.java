package aprsp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import aprsp.model.Transaction;
import aprsp.service.TransactionService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api")
public class TransactionController {

	private TransactionService transactionService;
	
	@Autowired
	public TransactionController(TransactionService transactionService) {
		// TODO Auto-generated constructor stub
		this.transactionService = transactionService;
	}
	
	@ApiOperation("Returns all transactions")
	@GetMapping("/transaction")
	public List<Transaction> getAll(){
		return transactionService.findAll();
	}
	
	@ApiOperation("Returns transaction by id")
	@GetMapping("/transaction/{id}")
	public ResponseEntity<Transaction> getOne(@PathVariable("id") Integer id) {
		Transaction transaction = transactionService.findOne(id);
		if(transaction != null)
			return new ResponseEntity<Transaction>(transaction, HttpStatus.OK);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@ApiOperation("Return transaction by bank account sender")
	@GetMapping("/transactionByAccountSender/{bankAccountSender}")
	public List<Transaction> getTransactionByBankAccountSender(@PathVariable("bankAccountSender") String bankAccountSender){
		return transactionService.findByBankAccountSender(bankAccountSender);
	}
	@ApiOperation("Return transaction by bank account receiver")
	@GetMapping("/transactionByAccountReceiver/{bankAccountReceiver}")
	public List<Transaction> getTransactionByBankAccountReceiver(@PathVariable("bankAccountReceiver") String bankAccountReceiver){
		return transactionService.findByBankAccountReceiver(bankAccountReceiver);
	}
	
	@ApiOperation("Insert transaction in database")
	@PostMapping("/transaction")
	public ResponseEntity<Transaction> insert(@RequestBody Transaction transaction){
		transactionService.save(transaction);
		return new ResponseEntity<>(transaction, HttpStatus.CREATED);
	}
	
	@ApiOperation("Delete transaction by id")
	@DeleteMapping("/transaction/{id}")
	public ResponseEntity<Void> delete(@PathVariable("id") Integer id){
		if(transactionService.findOne(id) == null)
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		transactionService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
