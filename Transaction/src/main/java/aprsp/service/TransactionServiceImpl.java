package aprsp.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import aprsp.model.Transaction;
import aprsp.repository.TransactionRepository;

@Service
public class TransactionServiceImpl implements TransactionService{

	private TransactionRepository transactionRepository;
	
	@Autowired
	public TransactionServiceImpl(TransactionRepository transactionRepository) {
		// TODO Auto-generated constructor stub
		this.transactionRepository = transactionRepository;
	}
	@Override
	public List<Transaction> findAll() {
		// TODO Auto-generated method stub
		return transactionRepository.findAll();
	}

	@Override
	public Transaction findOne(Integer id) {
		// TODO Auto-generated method stub
		return transactionRepository.findById(id).orElse(null);
	}

	@Override
	public List<Transaction> findByBankAccountSender(String bankAccountSender) {
		// TODO Auto-generated method stub
		return transactionRepository.findByBankAccountSender(bankAccountSender);
	}

	@Override
	public List<Transaction> findByBankAccountReceiver(String bankAccountReceiver) {
		// TODO Auto-generated method stub
		return transactionRepository.findByBankAccountReceiver(bankAccountReceiver);
	}

	@Override
	public Transaction save(Transaction transaction) {
		// TODO Auto-generated method stub
		if(transaction.getId() == null) {
			transaction.setId(transactionRepository.getGeneratedId());
			transaction.setDateCreated(Timestamp.valueOf(LocalDateTime.now()));
		}
		return transactionRepository.save(transaction);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		transactionRepository.deleteById(id);
	}

}
