package aprsp.service;

import java.util.List;

import org.springframework.stereotype.Component;

import aprsp.model.Transaction;

@Component
public interface TransactionService {

	List<Transaction> findAll();
	
	Transaction findOne(Integer id);
	
	List<Transaction> findByBankAccountSender(String bankAccountSender);
	
	List<Transaction> findByBankAccountReceiver(String bankAccountReceiver);
	
	Transaction save(Transaction transaction);
	
	void delete(Integer id);
}
