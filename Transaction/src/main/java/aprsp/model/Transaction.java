package aprsp.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the transaction database table.
 * 
 */
@Entity
@NamedQuery(name="Transaction.findAll", query="SELECT t FROM Transaction t")
public class Transaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TRANSACTION_ID_GENERATOR", sequenceName="transaction_id_seq")
    @Column(name = "id", updatable = false)
	private Integer id;

	private BigDecimal amount;

	@Column(name="bank_account_receiver")
	private String bankAccountReceiver;

	@Column(name="bank_account_sender")
	private String bankAccountSender;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="date_created", updatable = false)
	private Date dateCreated;

	private String purpose;

	public Transaction() {
	}
	
	@PrePersist
	protected void onCreate() {
	    dateCreated = new Date();
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getBankAccountReceiver() {
		return this.bankAccountReceiver;
	}

	public void setBankAccountReceiver(String bankAccountReceiver) {
		this.bankAccountReceiver = bankAccountReceiver;
	}

	public String getBankAccountSender() {
		return this.bankAccountSender;
	}

	public void setBankAccountSender(String bankAccountSender) {
		this.bankAccountSender = bankAccountSender;
	}

	public Date getDateCreated() {
		return this.dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getPurpose() {
		return this.purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

}