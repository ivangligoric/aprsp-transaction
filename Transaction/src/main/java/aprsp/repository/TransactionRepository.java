package aprsp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import aprsp.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer>{

	List<Transaction> findByBankAccountReceiver(String bankAccountReceiver);
	
	List<Transaction> findByBankAccountSender(String bankAccountSender);
	
	@Query(value = "select nextval('transaction_id_seq')", nativeQuery = true)
	Integer getGeneratedId();

}
